//
//  HospitalDetailsVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit

class HospitalDetailsVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_ReviewsList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_ReviewsList.delegate = self
        self.Tableview_ReviewsList.dataSource = self
        self.Tableview_ReviewsList.reloadData()
        self.Height_Tableview.constant = 2 * 80
    }
    

    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Tableview Methods
extension HospitalDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_ReviewsList.dequeueReusableCell(withIdentifier: "ReviewListCell") as! ReviewListCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

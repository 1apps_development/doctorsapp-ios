//
//  HospitalVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit

class HospitalVC: UIViewController {

    @IBOutlet weak var Tableview_HospitalLists: UITableView!
    
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Collectioview_CategoriesHospital: UICollectionView!
    
    var Array_Hospital_Name = ["Max Hospital, Delli","Doon Hospital, Mumbi"]
    var Array_Hospital_Image = ["ic_hosp01","ic_hosp02"]
    
    var Array_DoctorCategory = ["All","Kidney","Gastro Liver"]
    var Array_DoctorCategory_Image = ["ic_dotcat01","ic_dotcat02","ic_dotcat03"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectioview_CategoriesHospital.delegate = self
        self.Collectioview_CategoriesHospital.dataSource = self
        self.Collectioview_CategoriesHospital.reloadData()
        
        self.Tableview_HospitalLists.delegate = self
        self.Tableview_HospitalLists.dataSource = self
        self.Tableview_HospitalLists.reloadData()
        self.Height_Tableview.constant = CGFloat(self.Array_Hospital_Name.count * 100)
        
    }
                  

}
//MARK: Tableview Methods
extension HospitalVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array_Hospital_Name.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_HospitalLists.dequeueReusableCell(withIdentifier: "HospitalListCell") as! HospitalListCell
        cell.lbl_name.text = self.Array_Hospital_Name[indexPath.row]
        cell.lbl_categories.text = "Cardology"
        cell.img_images.image = UIImage(named: self.Array_Hospital_Image[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HospitalDetailsVC") as! HospitalDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
// MARK:- CollectionView Deleget methods

extension HospitalVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectioview_CategoriesHospital
        {
            return Array_DoctorCategory.count
        }
        
        else
        {
            return 0
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectioview_CategoriesHospital
        {
            let cell = self.Collectioview_CategoriesHospital.dequeueReusableCell(withReuseIdentifier: "DoctorCategoryCell", for: indexPath) as! DoctorCategoryCell
            cell.lbl_categoryname.text = self.Array_DoctorCategory[indexPath.row]
            cell.img_category.image = UIImage(named: self.Array_DoctorCategory_Image[indexPath.row])
            
            return cell
        }
        
        else
        {
            return UICollectionViewCell()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.Collectioview_CategoriesHospital
        {
            let label = UILabel(frame: CGRect.zero)
            label.text = self.Array_DoctorCategory[indexPath.item]
            label.sizeToFit()
            return CGSize(width: label.frame.width + 60, height: 50)
        }
        
        else
        {
            return CGSize.zero
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    
}

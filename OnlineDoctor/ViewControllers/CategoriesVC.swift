//
//  CategoriesVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 06/04/22.
//

import UIKit
class CategoriesCell : UICollectionViewCell
{
    @IBOutlet weak var Cell_view: UIView!
    @IBOutlet weak var img_images: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    
}
class CategoriesVC: UIViewController {

    @IBOutlet weak var Collectionview_Categorieslist: UICollectionView!
    var CategoriesList = ["Hear","Psychology","Dentist","Cardiologist","Liver","Eye","Lungs","Heart","Kidney","Nose"]
    var arrColors = ["BBDEFF","30C084","D7F1DF","F6F1FA","FFBABB","B6E1E5","F0F0F1","F4E0D8","C7CEF7","FFF1C0"] // Use your color hex
var Array_image = ["ic_cat01","ic_cat02","ic_cat03","ic_cat04","ic_cat05","ic_cat06","ic_cat07","ic_cat08","ic_cat09","ic_cat010"] // Use your color hex
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_Categorieslist.delegate = self
        self.Collectionview_Categorieslist.dataSource = self
        self.Collectionview_Categorieslist.reloadData()
    }
    
    @IBAction func btnTap_Continue(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
        appNavigation.setNavigationBarHidden(true, animated: true)
        keyWindow?.rootViewController = TabViewController
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
// MARK:- CollectionView Deleget methods
extension CategoriesVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.CategoriesList.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.Collectionview_Categorieslist.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        cell.lbl_title.text  = self.CategoriesList[indexPath.item]
        
        cell.Cell_view.backgroundColor = self.hexStringToUIColor(hex: self.arrColors[indexPath.item])
        cell.img_images.image = UIImage.init(named: self.Array_image[indexPath.row])
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (UIScreen.main.bounds.width - 60) / 3, height: 120)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indasexPath: IndexPath) {
        
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

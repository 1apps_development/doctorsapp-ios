//
//  IntroVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 06/04/22.
//

import UIKit

class IntroCell : UICollectionViewCell
{
    
    @IBOutlet weak var img_intro: UIImageView!
    @IBOutlet weak var btn_Next: UIButton!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    
}

class IntroVC: UIViewController {
    
    
    @IBOutlet weak var Collectionview_IntroList: UICollectionView!
    var titleArray = ["Easy consultation with a doctor in your pocket","Looking for a trusted & secured online Dr","Consult with the top doctors online just Call"]
    var SubtitleArray = ["A complane health app chat the allows customers get diagnased via chera phone","A complane health app chat the allows customers get diagnased via chera phone","A complane health app chat the allows customers get diagnased via chera phone"]
    var img_intro = ["ic_Picture01","ic_Picture02","ic_Picture03"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Collectionview_IntroList.delegate = self
        self.Collectionview_IntroList.dataSource = self
        self.Collectionview_IntroList.reloadData()
    }
    
}
// MARK:- CollectionView Deleget methods
extension IntroVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.Collectionview_IntroList.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        cell.lbl_title.text = titleArray[indexPath.item]
        cell.lbl_subtitle.text = SubtitleArray[indexPath.item]
        cell.btn_Next.tag = indexPath.row
        cell.btn_Next.addTarget(self, action:#selector(btnTap_Next), for: .touchUpInside)
        cell.img_intro.image = UIImage(named: self.img_intro[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (UIScreen.main.bounds.width - 10 ) / 1, height: collectionView.frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    @objc func btnTap_Next(sender:UIButton!)
    {
        if sender.tag == 2
        {
            print("Login")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let collectionBounds = self.Collectionview_IntroList.bounds
            let contentOffset = CGFloat(floor(self.Collectionview_IntroList.contentOffset.x + collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset)
        }
        
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.Collectionview_IntroList.contentOffset.y ,width : self.Collectionview_IntroList.frame.width,height : self.Collectionview_IntroList.frame.height)
        self.Collectionview_IntroList.scrollRectToVisible(frame, animated: true)
    }
}

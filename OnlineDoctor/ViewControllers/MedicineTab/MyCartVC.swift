//
//  MyCartVC.swift
//  OnlineDoctor
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit

class MyCartcell : UITableViewCell
{
    
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_itemname: UILabel!
    @IBOutlet weak var img_images: UIImageView!
}

class MyCartVC: UIViewController {

    @IBOutlet weak var height_tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_CartList: UITableView!
    
    var Array_Name = ["Gut Restore","Various Medicine"]
    var Array_price = ["$18.89","$36.74"]
    var Array_image = ["ic_cart01","ic_cart02"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_CartList.delegate = self
        self.Tableview_CartList.dataSource = self
        self.Tableview_CartList.reloadData()
        self.height_tableview.constant = CGFloat(self.Array_Name.count * 110)
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTap_Payment(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.iscomefrom = "1"
        self.present(vc, animated: false, completion: nil)
    }
    
}

//MARK: Tableview Methods
extension MyCartVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Array_Name.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_CartList.dequeueReusableCell(withIdentifier: "MyCartcell") as! MyCartcell
        cell.lbl_price.text = self.Array_price[indexPath.row]
        cell.lbl_itemname.text = self.Array_Name[indexPath.row]
        cell.img_images.image = UIImage.init(named: self.Array_image[indexPath.row])
        cornerRadius(viewName: cell.lbl_qty, radius: cell.lbl_qty.frame.height / 2)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

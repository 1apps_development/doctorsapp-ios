//
//  FindMedicineVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit


class FindMedicineVC: UIViewController {
    @IBOutlet weak var Collectionview_MedicineCategoriesList: UICollectionView!
    @IBOutlet weak var Collectionview_MedicineList: UICollectionView!
    
    @IBOutlet weak var Height_Collectionview: NSLayoutConstraint!
    
    var Array_DoctorCategory = ["All","Cold","Fever"]
    var Array_DoctorCategory_Image = ["ic_dotcat01","ic_dotcat02","ic_dotcat03"]
    
    var Array_Doctor_Name = ["$2.30","$2.30","$2.30","$2.30","$2.30","$2.30"]
    var Array_Doctor_Categories = ["Napa Extra","Napa Extra","Napa Extra","Napa Extra","Napa Extra","Napa Extra"]
    var Array_Doctor_Image = ["ic_med01","ic_med02","ic_med03","ic_med01","ic_med02","ic_med03"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_MedicineCategoriesList.delegate = self
        self.Collectionview_MedicineCategoriesList.dataSource = self
        self.Collectionview_MedicineCategoriesList.reloadData()
        
        self.Collectionview_MedicineList.delegate = self
        self.Collectionview_MedicineList.dataSource = self
        self.Collectionview_MedicineList.reloadData()
        self.Height_Collectionview.constant = 3.5 * ((UIScreen.main.bounds.width - 55) / 2) + 15
        
    }
    @IBAction func btnTap_Cart(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
// MARK:- CollectionView Deleget methods

extension FindMedicineVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_MedicineCategoriesList
        {
            return Array_DoctorCategory.count
        }
        else if collectionView == self.Collectionview_MedicineList
        {
            return Array_Doctor_Name.count
        }
        else
        {
            return 0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectionview_MedicineCategoriesList
        {
            let cell = self.Collectionview_MedicineCategoriesList.dequeueReusableCell(withReuseIdentifier: "DoctorCategoryCell", for: indexPath) as! DoctorCategoryCell
            cell.lbl_categoryname.text = self.Array_DoctorCategory[indexPath.row]
            cell.img_category.image = UIImage(named: self.Array_DoctorCategory_Image[indexPath.row])
            return cell
        }
        else if collectionView == self.Collectionview_MedicineList
        {
            let cell = self.Collectionview_MedicineList.dequeueReusableCell(withReuseIdentifier: "DoctorOrderCell", for: indexPath) as! DoctorOrderCell
            cell.lbl_name.text = self.Array_Doctor_Name[indexPath.row]
            cell.lbl_categories.text = self.Array_Doctor_Categories[indexPath.row]
            cell.img_images.image = UIImage(named: self.Array_Doctor_Image[indexPath.row])
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.Collectionview_MedicineCategoriesList
        {
            let label = UILabel(frame: CGRect.zero)
            label.text = self.Array_DoctorCategory[indexPath.item]
            label.sizeToFit()
            return CGSize(width: label.frame.width + 60, height: 50)
        }
        else if collectionView == self.Collectionview_MedicineList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 55) / 2, height: ((UIScreen.main.bounds.width - 55) / 2) + 15)
        }
        else
        {
            return CGSize.zero
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MedicineDetailsVC") as! MedicineDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

//
//  ConfirmOrderVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit

class ConfirmOrderVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTap_continueShopping(_ sender: Any) {
        
        
    }
    
    @IBAction func btnTap_Backtohome(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
        appNavigation.setNavigationBarHidden(true, animated: true)
        keyWindow?.rootViewController = TabViewController
    }
    
}

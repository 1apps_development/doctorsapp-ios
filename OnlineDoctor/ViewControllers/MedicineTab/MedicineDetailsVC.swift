//
//  MedicineDetailsVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit

class MedicineDetailsVC: UIViewController {
    @IBOutlet weak var Collectionview_MedicineList: UICollectionView!
    var Array_Doctor_Name = ["$2.30","$2.30","$2.30","$2.30","$2.30","$2.30"]
    var Array_Doctor_Categories = ["Napa Extra","Napa Extra","Napa Extra","Napa Extra","Napa Extra","Napa Extra"]
    var Array_Doctor_Image = ["ic_med01","ic_med02","ic_med03","ic_med01","ic_med02","ic_med03"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Collectionview_MedicineList.dataSource = self
        self.Collectionview_MedicineList.delegate = self
        self.Collectionview_MedicineList.reloadData()
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    


}
// MARK:- CollectionView Deleget methods

extension MedicineDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_MedicineList
        {
            return Array_Doctor_Name.count
        }
        else
        {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.Collectionview_MedicineList
        {
            let cell = self.Collectionview_MedicineList.dequeueReusableCell(withReuseIdentifier: "DoctorOrderCell", for: indexPath) as! DoctorOrderCell
            cell.lbl_name.text = self.Array_Doctor_Name[indexPath.row]
            cell.lbl_categories.text = self.Array_Doctor_Categories[indexPath.row]
            cell.img_images.image = UIImage(named: self.Array_Doctor_Image[indexPath.row])
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         if collectionView == self.Collectionview_MedicineList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 55) / 2, height: ((UIScreen.main.bounds.width - 55) / 2) + 15)
        }
        else
        {
            return CGSize.zero
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

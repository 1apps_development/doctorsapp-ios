//
//  NotificatonVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 08/04/22.
//

import UIKit

class NotificationCell : UITableViewCell
{
    
}

class NotificatonVC: UIViewController {

    @IBOutlet weak var TableView_NotificationList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableView_NotificationList.delegate = self
        self.TableView_NotificationList.dataSource = self
        self.TableView_NotificationList.reloadData()
       
    }
    

    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Tableview Methods
extension NotificatonVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_NotificationList.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

//
//  CountryCodeListVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 06/04/22.
//

import UIKit
class CountryListCell : UITableViewCell
{
    
    @IBOutlet weak var lbl_countryCode: UILabel!
    @IBOutlet weak var lbl_countryName: UILabel!
    @IBOutlet weak var img_countryFlag: UIImageView!
}
class CountryCodeListVC: UIViewController {
    @IBOutlet weak var Tableview_CountryList: UITableView!
    var Array_image = ["01","02","03","04","05","06"]
    var Array_Name = ["U.S.A","Saudi Arabia","Indonesia","Malaysia","Bangladesh","Australia"]
    var Array_Code = ["+1","+966","+62","+60","+88","+61"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_CountryList.delegate = self
        self.Tableview_CountryList.dataSource = self
        self.Tableview_CountryList.reloadData()
        
    }
    
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK: Tableview Methods
extension CountryCodeListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Array_image.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_CountryList.dequeueReusableCell(withIdentifier: "CountryListCell") as! CountryListCell
        cell.img_countryFlag.image = UIImage.init(named: self.Array_image[indexPath.row])
        cell.lbl_countryCode.text = self.Array_Code[indexPath.row]
        cell.lbl_countryName.text = self.Array_Name[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

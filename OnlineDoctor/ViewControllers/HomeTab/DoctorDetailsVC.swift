//
//  DoctorDetailsVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 07/04/22.
//

import UIKit

class ReviewListCell : UITableViewCell
{
    
}
class DoctorDetailsVC: UIViewController {
    
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_reviewList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_reviewList.delegate = self
        self.Tableview_reviewList.dataSource = self
        self.Tableview_reviewList.reloadData()
        self.Height_Tableview.constant = 2 * 80
        
        
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTap_MakeAppointment(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleVC") as! ScheduleVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func btnTap_OnlineConsultation(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleVC") as! ScheduleVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    
    
}
//MARK: Tableview Methods
extension DoctorDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_reviewList.dequeueReusableCell(withIdentifier: "ReviewListCell") as! ReviewListCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

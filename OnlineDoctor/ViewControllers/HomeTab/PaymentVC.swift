//
//  PaymentVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 07/04/22.
//

import UIKit

class paymentcell : UITableViewCell
{
    @IBOutlet weak var img_images: UIImageView!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
}
class PaymentVC: UIViewController {

    @IBOutlet weak var Tableview_PaymentList: UITableView!
    var iscomefrom = String()
    var Array_title = ["Credit Card","Paypal"]
    var Array_Subtitle = ["XXXXXX1938","paypal@gmail.com"]
    var Array_images = ["ic_payment01","ic_payment02"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_PaymentList.delegate = self
        self.Tableview_PaymentList.dataSource = self
        self.Tableview_PaymentList.reloadData()
    }

    @IBAction func btnTap_Confirm(_ sender: UIButton) {
        if self.iscomefrom == "1"
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmOrderVC") as! ConfirmOrderVC
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
        
    }
    

}
//MARK: Tableview Methods
extension PaymentVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array_title.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_PaymentList.dequeueReusableCell(withIdentifier: "paymentcell") as! paymentcell
        cell.lbl_title.text = self.Array_title[indexPath.row]
        cell.lbl_subtitle.text = self.Array_Subtitle[indexPath.row]
        cell.img_images.image = UIImage(named: self.Array_images[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

//
//  ScheduleVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 07/04/22.
//

import UIKit

class ScheduleVC: UIViewController {
    @IBOutlet weak var tagListView1: TagListView!
    @IBOutlet weak var tagListView2: TagListView!
    @IBOutlet weak var tagListView3: TagListView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tagListView1.delegate = self
        tagListView1.addTags(["Morning", "Afternoon", "Evening"])
        tagListView1.alignment = .left
        tagListView1.textFont = UIFont(name: "Saira Medium", size: 14)!
        tagListView1.borderWidths = 1
        tagListView1.layer.borderWidth = 0.0
        
        tagListView2.delegate = self
        tagListView2.addTags(["02:30 pm", "02:40 pm", "02:50 pm"])
        tagListView2.alignment = .left
        tagListView2.textFont = UIFont(name: "Saira Medium", size: 14)!
        tagListView2.borderWidths = 1
        tagListView2.layer.borderWidth = 0.0
        
        tagListView3.delegate = self
        tagListView3.addTags(["Online", "Offline"])
        tagListView3.alignment = .left
        tagListView3.textFont = UIFont(name: "Saira Medium", size: 14)!
        tagListView3.borderWidths = 1
        tagListView3.layer.borderWidth = 0.0
        
    }
    
    @IBAction func btnTap_OnlineConsultation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConsultMethodVC") as! ConsultMethodVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
        
    }
    

}
extension ScheduleVC : TagListViewDelegate
{
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }

    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}

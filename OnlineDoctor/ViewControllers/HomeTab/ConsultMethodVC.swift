//
//  ConsultMethodVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 07/04/22.
//

import UIKit

class ConsultmethodsCell : UITableViewCell
{
    @IBOutlet weak var img_images: UIImageView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
}

class ConsultMethodVC: UIViewController {

    @IBOutlet weak var Tableview_PricingList: UITableView!
    
    var Array_title = ["Messaging","Voice Call","Video Call"]
    var Array_Subtitle = ["Messaging with doctor","Make a voice call with doctor","Make a video call with doctor"]
    var Array_images = ["ic_Cons01","ic_Cons02","ic_Cons03"]
    var Array_price = ["$10","$20","$30"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_PricingList.delegate = self
        self.Tableview_PricingList.dataSource = self
        self.Tableview_PricingList.reloadData()
    }

    @IBAction func btnTap_Paynow(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
}
//MARK: Tableview Methods
extension ConsultMethodVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array_title.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_PricingList.dequeueReusableCell(withIdentifier: "ConsultmethodsCell") as! ConsultmethodsCell
        cell.lbl_title.text = self.Array_title[indexPath.row]
        cell.lbl_subtitle.text = self.Array_Subtitle[indexPath.row]
        cell.lbl_price.text = self.Array_price[indexPath.row]
        cell.img_images.image = UIImage(named: self.Array_images[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

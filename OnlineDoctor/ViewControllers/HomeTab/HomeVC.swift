//
//  HomeVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 06/04/22.
//

import UIKit
class DoctorCategoryCell : UICollectionViewCell
{
    
    @IBOutlet weak var lbl_categoryname: UILabel!
    @IBOutlet weak var img_category: UIImageView!
}
class DoctorOrderCell : UICollectionViewCell
{
    
    @IBOutlet weak var lbl_categories: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_images: UIImageView!
}
class HospitalListCell : UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_images: UIImageView!
    @IBOutlet weak var lbl_categories: UILabel!
}
class HomeVC: UIViewController {
    
    @IBOutlet weak var Collectionview_DoctorCategoriesList: UICollectionView!
    @IBOutlet weak var Collectionview_DoctorList: UICollectionView!
    @IBOutlet weak var Collectionview_HospitalCategoriesList: UICollectionView!
    
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_HospitalList: UITableView!
    
    
    var Array_DoctorCategory = ["All","Cardiology","Neurology"]
    var Array_DoctorCategory_Image = ["ic_dotcat01","ic_dotcat02","ic_dotcat03"]
    
    var Array_Doctor_Name = ["Dr. Marian","Dr. Arpona","Dr. Humar"]
    var Array_Doctor_Categories = ["Cardiology","Neurology","Hepotology"]
    var Array_Doctor_Image = ["ic_doct01","ic_doct02","ic_doct03"]
    
    var Array_HospitalCategory = ["Doctor List","Hospital","Medicine"]
    var Array_HospitalCategory_Image = ["ic_hos01","ic_hos02","ic_hos03"]
    
    
    var Array_Hospital_Name = ["Max Hospital, Delli","Doon Hospital, Mumbi"]
    var Array_Hospital_Image = ["ic_hosp01","ic_hosp02"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Collectionview_DoctorCategoriesList.delegate = self
        self.Collectionview_DoctorCategoriesList.dataSource = self
        self.Collectionview_DoctorCategoriesList.reloadData()
        
        self.Collectionview_DoctorList.delegate = self
        self.Collectionview_DoctorList.dataSource = self
        self.Collectionview_DoctorList.reloadData()
        
        
        
        self.Collectionview_HospitalCategoriesList.delegate = self
        self.Collectionview_HospitalCategoriesList.dataSource = self
        self.Collectionview_HospitalCategoriesList.reloadData()
        
        self.Tableview_HospitalList.delegate = self
        self.Tableview_HospitalList.dataSource = self
        self.Tableview_HospitalList.reloadData()
        self.Height_Tableview.constant = CGFloat(self.Array_Hospital_Name.count * 100)
        
    }
    
    @IBAction func btnTap_Notifcaiton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificatonVC") as! NotificatonVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK:- CollectionView Deleget methods

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_DoctorCategoriesList
        {
            return Array_DoctorCategory.count
        }
        else if collectionView == self.Collectionview_DoctorList
        {
            return Array_Doctor_Name.count
        }
        else if collectionView == self.Collectionview_HospitalCategoriesList
        {
            return Array_HospitalCategory.count
        }
        else
        {
            return 0
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectionview_DoctorCategoriesList
        {
            let cell = self.Collectionview_DoctorCategoriesList.dequeueReusableCell(withReuseIdentifier: "DoctorCategoryCell", for: indexPath) as! DoctorCategoryCell
            cell.lbl_categoryname.text = self.Array_DoctorCategory[indexPath.row]
            cell.img_category.image = UIImage(named: self.Array_DoctorCategory_Image[indexPath.row])
            
            return cell
        }
        else if collectionView == self.Collectionview_DoctorList
        {
            let cell = self.Collectionview_DoctorList.dequeueReusableCell(withReuseIdentifier: "DoctorOrderCell", for: indexPath) as! DoctorOrderCell
            cell.lbl_name.text = self.Array_Doctor_Name[indexPath.row]
            cell.lbl_categories.text = self.Array_Doctor_Categories[indexPath.row]
            cell.img_images.image = UIImage(named: self.Array_Doctor_Image[indexPath.row])
            return cell
        }
        else if collectionView == self.Collectionview_HospitalCategoriesList
        {
            let cell = self.Collectionview_HospitalCategoriesList.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            cell.lbl_title.text = self.Array_HospitalCategory[indexPath.row]
            cell.img_images.image = UIImage(named: self.Array_HospitalCategory_Image[indexPath.row])
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.Collectionview_DoctorCategoriesList
        {
            let label = UILabel(frame: CGRect.zero)
            label.text = self.Array_DoctorCategory[indexPath.item]
            label.sizeToFit()
            return CGSize(width: label.frame.width + 55, height: 50)
        }
        else if collectionView == self.Collectionview_DoctorList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 20) / 3, height: 175)
        }
        else if collectionView == self.Collectionview_HospitalCategoriesList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 20) / 3, height: 180)
        }
        else
        {
            return CGSize.zero
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.Collectionview_DoctorList
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorDetailsVC") as! DoctorDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
//MARK: Tableview Methods
extension HomeVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Array_Hospital_Name.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_HospitalList.dequeueReusableCell(withIdentifier: "HospitalListCell") as! HospitalListCell
        cell.lbl_name.text = self.Array_Hospital_Name[indexPath.row]
        cell.lbl_categories.text = "Cardology"
        cell.img_images.image = UIImage(named: self.Array_Hospital_Image[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HospitalDetailsVC") as! HospitalDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

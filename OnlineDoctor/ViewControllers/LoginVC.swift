//
//  LoginVC.swift
//  OnlineDoctor
//
//  Created by DREAMWORLD on 06/04/22.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad(){
        super.viewDidLoad()
        
    }
    
    @IBAction func btnTap_countySelect(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeListVC") as! CountryCodeListVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVarificationVC") as! OTPVarificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
